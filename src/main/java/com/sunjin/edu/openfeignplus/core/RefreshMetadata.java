package com.sunjin.edu.openfeignplus.core;

/**
 * RefreshMetadata
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class RefreshMetadata {

    private String clientName;

    private String url;

    private int connectTimeout;

    private int readTimeout;

    public RefreshMetadata(String clientName, String url) {
        this.clientName = clientName;
        this.url = url;
    }

    public RefreshMetadata(String clientName, String url, int connectTimeout, int readTimeout) {
        this.clientName = clientName;
        this.url = url;
        this.connectTimeout = connectTimeout;
        this.readTimeout = readTimeout;
    }

    public String getClientName() {
        return clientName;
    }


    public String getUrl() {
        return url;
    }


    public int getConnectTimeout() {
        return connectTimeout;
    }


    public int getReadTimeout() {
        return readTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }
}
