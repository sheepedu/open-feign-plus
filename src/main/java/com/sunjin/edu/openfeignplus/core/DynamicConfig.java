package com.sunjin.edu.openfeignplus.core;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * DynamicConfig
 *
 * @author: sunj
 * @since 1.0.0
 **/
@Component
public class DynamicConfig {

    @Value("${config.refreshMetadataMap}")
    private String refreshMetadataMapStr;


    public Map<String, RefreshMetadata> getRefreshMetadataMap() {

        Map<String, RefreshMetadata> refreshMetadataMap = new HashMap<>();
        JSONArray array = JSON.parseArray(getRefreshMetadataMapStr());

        for (Object o : array) {
            JSONObject jsonObject = JSON.parseObject(o.toString());

            Set<Map.Entry<String, Object>> entries = jsonObject.entrySet();
            for (Map.Entry<String, Object> entry : entries) {
                String key = entry.getKey();
                RefreshMetadata refreshMetadata = JSON.parseObject(entry.getValue().toString(), RefreshMetadata.class);
                refreshMetadataMap.put(key,refreshMetadata);
            }
        }
        return refreshMetadataMap;
    }

    public String getRefreshMetadataMapStr() {
        return refreshMetadataMapStr;
    }

}
