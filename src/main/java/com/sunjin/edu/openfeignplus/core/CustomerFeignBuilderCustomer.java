package com.sunjin.edu.openfeignplus.core;

import feign.Feign;
import feign.micrometer.MicrometerCapability;
import org.springframework.cloud.openfeign.FeignBuilderCustomizer;

/**
 * CustomerFeignBuilderCustomer
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class CustomerFeignBuilderCustomer implements FeignBuilderCustomizer {

    private RefreshCapability refreshCapability;

    public CustomerFeignBuilderCustomer(RefreshCapability refreshCapability) {
        this.refreshCapability = refreshCapability;
    }

    @Override
    public void customize(Feign.Builder builder) {
        builder.addCapability(refreshCapability).addCapability(new MicrometerCapability());
    }
}
