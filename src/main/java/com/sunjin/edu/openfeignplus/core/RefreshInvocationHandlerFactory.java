package com.sunjin.edu.openfeignplus.core;

import feign.InvocationHandlerFactory;
import feign.Request;
import feign.Target;
import org.springframework.cloud.openfeign.FeignClient;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;
import java.util.Map;

/**
 * NewInvocationHandler
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class RefreshInvocationHandlerFactory implements InvocationHandlerFactory {
    private InvocationHandlerFactory delegateFactory;
    private DynamicConfig dynamicConfig;
    public RefreshInvocationHandlerFactory(InvocationHandlerFactory invocationHandlerFactory, DynamicConfig dynamicConfig) {
        this.delegateFactory = invocationHandlerFactory;
        this.dynamicConfig = dynamicConfig;
    }
    @Override
    public InvocationHandler create(Target target, Map<Method, MethodHandler> dispatch) {
        final InvocationHandler delegateHandler = delegateFactory.create(target, dispatch);
        return (proxy, method, args) ->  {
            Class<?> declaringClass = method.getDeclaringClass();
            FeignClient feignClient = declaringClass.getAnnotation(FeignClient.class);
            String name = feignClient.name();
            Map<String, RefreshMetadata> refreshMetadataMap = dynamicConfig.getRefreshMetadataMap();
            Object[] copy = Arrays.copyOf(args, args.length + 2);
            if(!Arrays.stream(args).allMatch(e ->e.getClass().isAssignableFrom(URI.class))){
                copy[copy.length-2] = URI.create(refreshMetadataMap.get(name).getUrl());
            }
            if(!Arrays.stream(args).allMatch(e ->e.getClass().isAssignableFrom(Request.Options.class))){
                copy[copy.length-1] = new Request.Options(refreshMetadataMap.get(name).getConnectTimeout(),refreshMetadataMap.get(name).getReadTimeout());
            }
            return  delegateHandler.invoke(proxy,method,copy);
        };
    }
}
