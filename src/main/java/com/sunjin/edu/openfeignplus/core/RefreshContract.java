package com.sunjin.edu.openfeignplus.core;

import com.sunjin.edu.openfeignplus.annotation.Refresh;
import feign.MethodMetadata;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringMvcContract;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * RefreshContract
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class RefreshContract extends SpringMvcContract {


    @Override
    protected void processAnnotationOnClass(MethodMetadata data, Class<?> clz) {
        super.processAnnotationOnClass(data,clz);
    }

    @Override
    protected void processAnnotationOnMethod(MethodMetadata data, Annotation annotation, Method method) {
        super.processAnnotationOnMethod(data,annotation,method);
        Class<?> declaringClass = method.getDeclaringClass();
        if(method.isAnnotationPresent(Refresh.class) && declaringClass.isAnnotationPresent(FeignClient.class)){
            data.urlIndex(method.getParameterCount());

            FeignClient feignClient = declaringClass.getAnnotation(FeignClient.class);
            String name = feignClient.name();
            String url = feignClient.url();

            RefreshMetadataStore.setRefreshMetadataMap(name,new RefreshMetadata(name,url));
        }
    }

    @Override
    protected boolean processAnnotationsOnParameter(MethodMetadata data, Annotation[] annotations, int paramIndex) {
        return super.processAnnotationsOnParameter(data,annotations,paramIndex);
    }


}
