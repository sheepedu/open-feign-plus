package com.sunjin.edu.openfeignplus.core;

import feign.Capability;
import feign.Contract;
import feign.InvocationHandlerFactory;

/**
 * RefreshCapability
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class RefreshCapability implements Capability {


    private DynamicConfig dynamicConfig;

    public RefreshCapability(DynamicConfig dynamicConfig) {
        this.dynamicConfig = dynamicConfig;
    }

    @Override
    public Contract enrich(Contract contract) {
        return new RefreshContract();
    }

    @Override
    public InvocationHandlerFactory enrich(InvocationHandlerFactory invocationHandlerFactory) {
        return new RefreshInvocationHandlerFactory(invocationHandlerFactory,dynamicConfig);
    }


}
