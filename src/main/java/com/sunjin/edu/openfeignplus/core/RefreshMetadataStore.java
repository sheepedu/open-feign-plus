package com.sunjin.edu.openfeignplus.core;

import java.util.HashMap;
import java.util.Map;

/**
 * RefreshMetadataStore
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class RefreshMetadataStore {

    private static Map<String,RefreshMetadata> refreshMetadataMap = new HashMap<>();

    public static RefreshMetadata getRefreshMetadataMap(String name){
        return refreshMetadataMap.get(name);
    }

    public static void setRefreshMetadataMap(String name,RefreshMetadata refreshMetadata){
        refreshMetadataMap.put(name,refreshMetadata);
    }
}
