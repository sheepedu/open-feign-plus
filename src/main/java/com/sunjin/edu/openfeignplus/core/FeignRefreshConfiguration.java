package com.sunjin.edu.openfeignplus.core;

import org.springframework.context.annotation.Bean;

/**
 * FeignRefreshConfiguration
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class FeignRefreshConfiguration {

    @Bean
    public RefreshCapability refreshCapability(DynamicConfig dynamicConfig){
        return new RefreshCapability(dynamicConfig);
    }

    @Bean
    public CustomerFeignBuilderCustomer customerFeignBuilderCustomer(RefreshCapability refreshCapability){
        return new CustomerFeignBuilderCustomer(refreshCapability);
    }
}
