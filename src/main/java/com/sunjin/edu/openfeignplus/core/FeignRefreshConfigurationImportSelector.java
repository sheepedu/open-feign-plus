package com.sunjin.edu.openfeignplus.core;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * FeignRefreshConfiguration
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class FeignRefreshConfigurationImportSelector implements ImportSelector {


    private final String feignRefreshConfiguration = "com.sunjin.edu.openfeignplus.core.FeignRefreshConfiguration";

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{feignRefreshConfiguration};
    }
}
