package com.sunjin.edu.openfeignplus;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import com.sunjin.edu.openfeignplus.annotation.EnableFeignRefresh;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.sunjin.edu.openfeignplus.demo.feign")
@SpringBootApplication
@EnableApolloConfig
@EnableFeignRefresh
public class OpenFeignPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenFeignPlusApplication.class, args);
    }
}
